import { Link } from "react-router-dom"
import {Button} from "antd"
import Style from "./index.module.less"

const CustomButton = (props) => {
	const fn = props.customBtnEvent || function(){}
	return (
		<div to={props.path} className={Style['custom-button']}>
			<Button onClick={fn}>
				{ props.name }
			</Button>
		</div>
	)
}


export default CustomButton
