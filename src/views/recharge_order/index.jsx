

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import zsk from '../../static/icon-quickly-pay.png'
import usdt from '../../static/icon-usdt-shield.png'
import ms from '../../static/icon-cvs-pay.png'
import right from '../../static/right.png'

const TIME = 60000; //定义时效性


const DongTai = (props) => {
	const navigate = useNavigate()
	const [active, serActive] = useState(1);
	const [price, sePrice] = useState('');
	const [visible, setVisible] = useState(false)

	const changeTab = (current) => {
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}

	const items = [
		[
			{ id: 1, label: '轉數快-b', amount: 100 },
			{ id: 2, label: '轉數快-a', amount: 200 },
			{ id: 3, label: '轉數快-c', amount: 500 },
			{ id: 4, label: '轉數快-b', amount: 1000 },
		],
		[
			{ id: 5, label: '轉數快-a', amount: 2000 },
			{ id: 6, label: '轉數快-c', amount: 5000 },
			{ id: 7, label: '轉數快-b', amount: 10000 },
			{ id: 8, label: '轉數快-a', amount: 50000 },
		]
		
		// Add more items as needed
	];
	const select=(id,money)=>{
		console.log(id,money)
		sePrice(money)
	}



	return (
		<React.Fragment>
			<HeaderTitle title="充值訂單" right_title="首页" />
			<div className={style['login_box']}>

				{/* <div className={`${style['tabs']}`} onClick={() => {
					changeTab(1)
				}}>
					一键回收
				</div> */}
				<div className={style['flex']} style={{ justifyContent: 'space-between', padding: '10px 10px',marginBottom:'10px'}}>
					<div className={style['flex']} style={{ fontSize: '12px' }}>
						{/* <div className={style['left_label']}></div> */}
						<div style={{ letterSpacing: '2px' }}>帳號：<span style={{marginLeft:'5px'}}>babyaa88</span></div>
					</div>
					<div className={style['flex']} style={{ marginLeft: '20px', fontSize: '12px' }}>
						餘額：<span style={{ color: 'red' }}>0.00</span>元
					</div>
				</div>

				<div className={style['flex']} style={{ backgroundColor: '#fff', padding: '10px', border: '1px solid #E1E1E1' }}>
					<div className={style['flex']}>
						{/* <div className={style['left_label']}></div> */}
						<div style={{ letterSpacing: '1px' }}>充值金額</div>
					</div>
					<div className={style['flex']} style={{ marginLeft: '20px', fontSize: '12px', color: '#9d7201' }}>
						<input type="text" className={style['input']} placeholder="100-50000元" value={price}/>
					</div>
				</div>


				

				<div className={style['tablebady']}>
					<div className={style['tablebadyul']}>
					
					{items.map((item, index)=>{
						return (
							<div className={style['tablebadyli']} key={index}>
								{item.map((item2, index1)=>{
									return <span key={item2.id} onClick={()=>{
										select(item2.id,item2.amount)
									}}>{item2.amount}</span>
								})}
			
						</div>
						)
						})}
					
						
						{/* <div className={style['tablebadyli']}>
							<span>2000</span>
							<span>5000</span>
							<span>10000</span>
							<span>50000</span>
						</div> */}
					</div>
				</div>

				<div className={style['flex']} style={{ backgroundColor: '#fff', padding: '10px', border: '1px solid #E1E1E1' }}>
					<div className={style['flex']}>
						{/* <div className={style['left_label']}></div> */}
						<div style={{ letterSpacing: '1px' }}>充值渠道</div>
					</div>
					<div className={style['flex']} style={{ marginLeft: '20px', fontSize: '12px', color: '#9d7201', visibility: 'hidden' }}>
						<input type="text" className={style['input']} placeholder="100-50000元" />
					</div>
				</div>

				<div className={style['operation3']}>
					{/* {[style['tabs'], style['active']].join(' ')} */}

					<div className={`${style['tabs']} ${active === 1 ? style['active'] : ''}`} onClick={() => {
						changeTab(1)
					}}>
						<img src={zsk} alt="" style={{ width: '30px', marginBottom: '5px' }} />
						轉數快
					</div>
					{/* <div className={`${style['tabs']} ${active === 2 ? style['active'] : ''}`} onClick={()=>{
						changeTab(2)
					}}>
      2
            </div> */}
				</div>

				<div className={style['content1']} style={{color:'#1E901E'}}>

					* 轉賬時請將我方收款銀行，設為轉數快預設收款銀行，如因設置不正確導致轉賬錯誤，本公司一概不承擔。

				</div>

				<div className={style['content1']} style={{color:'#DF6262'}}>

				* 首沖即送20%,充￥1000送200，系統根據最新一筆充值金額審核優惠申請。

				</div>
				<div className={style['content1']} style={{color:'#B2B3B4'}}>

				* 轉速快小數點後兩位為系統配發的隨機數，轉賬時清準確輸入系統配發的金額，以免造成延誤或損失。

				</div>















			</div>


		</React.Fragment>


	)
}


export default DongTai
