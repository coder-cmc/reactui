import style from "./index.module.less"
import {LeftOutline} from "antd-mobile-icons"

const HeaderTitle = (props) => {
  return (
    <header className={style['header-title']}>
      <h1>
        <LeftOutline className={style['icon']} onClick={() => window.history.back()}/>
        {props.title}</h1>
        <span className={style['icon-right']}>{props.right_title}</span>
    </header>
  )
}

export default HeaderTitle