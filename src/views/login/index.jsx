

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "./header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import logo from '../../static/logo.png'

const TIME = 60000; //定义时效性


const Login = (props) => {
	const navigate = useNavigate()
	const [visible, setVisible] = useState(false)
	const [formData, setFormData] = useState({ name: '', password: '', checked: false })
	const [form] = Form.useForm()
	// 查看缓存中是否有账号密码
	let formDataMemo = {}
	if(localStorage.getItem('formData')){
		formDataMemo = JSON.parse(localStorage.getItem('formData'))
	}

	if (formDataMemo.name && formDataMemo.password && formDataMemo.expirationTime && Date.now() < formDataMemo.expirationTime) {
		setFormData({
			name: formDataMemo.name,
			password: formDataMemo.password,
			checked: true,
		})

		console.log(formData, 'formData')
	}

	const onFinish = (values) => {
		console.log(values, 'values')
		/*
			1.调用登录接口
			2.登录成功之后，看其是否勾选了记住密码，如果记住密码则将账号密码缓存在内存中
			3.下次进入秘密
		*/
		login(values).then(res => {
			if (res.code == 200) {
				Toast.show({
					icon: 'success',
					content: "操作成功",
				})

				// 如果设置了记住密码，则在登录后将账号密码存储在cookie或storage中，并设置时效性以此来记住密码
				if (formData.checked) {
					localStorage.setItem('formData', JSON.stringify({
						...values,
						expirationTime: Date.now() + TIME,
					}));
				} else {
					localStorage.removeItem('formData')
				}
			} else {
				Toast.show({
					icon: 'fail',
					content: "操作失败",
				})

			}
		})

	};

	const onFinishFailed = (errorInfo) => {
		console.log('Failed:', errorInfo);
	};



	return (
		<React.Fragment>
			<HeaderTitle title="登 入"/>
			<div className={style['login_box']}>
				<div style={{ textAlign: 'center' }}>
					<img src={logo} alt="" />
				</div>
				<Form
					form={form}
					layout='horizontal'
					onFinish={onFinish}
					initialValues={formData}
				>
					<Form.Item
						label='账 号'
						name='name'
						rules={[{ required: true, message: '账号不能为空!' }, { pattern: /^(?=\w*\d)(?=\w*[a-zA-Z])[\w]{6,12}$/, message: '请输入6-12位数字和字母组合' }]}
					>
						<Input placeholder='6-12个英文字母和数字' />
					</Form.Item>
					<Form.Item
						label='密 码'
						name='password'
						rules={[{ required: true, message: '密码不能为空!' }]}
						extra={
							<div>
								{!visible ? (
									<EyeInvisibleOutline onClick={() => setVisible(true)} />
								) : (
									<EyeOutline onClick={() => setVisible(false)} />
								)}
							</div>
						}>
						<Input placeholder='请输入密码' type={visible ? 'text' : 'password'} />
					</Form.Item>

				</Form>
				<div className={style['operation']}>
					<span>
						<Checkbox onChange={e => setFormData({ ...formData, checked:e })} defaultChecked={formData.checked}>
							记住密码
						</Checkbox>
					</span>
					<span onClick={() => navigate('/emptyPage')}>忘记密码</span>
				</div>
				<React.Fragment>
					<Button shape='rounded' className={style['btns']} style={{ marginBottom: '15px', marginTop: '15px' }} block onClick={() => {
						console.log(form, 'fomr')
						form.validateFields()
						form.submit()
					}} color='primary'>
						登录
					</Button>
					<Button shape='rounded' className={style['btns']} block type='submit' color='primary' onClick={() => navigate('/register')}>
						注册
					</Button>
					<p className={style['other-info']} onClick={() => navigate('/emptyPage')}>先去逛逛</p>

				</React.Fragment>
			</div>
		</React.Fragment>

	)
}


export default Login
