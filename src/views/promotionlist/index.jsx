

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import banner1 from "../../static/1.jpg"
import banner2 from "../../static/2.jpg"
import banner3 from "../../static/3.jpg"
import banner4 from "../../static/4.jpg"

const Promotionlist = (props) => {
	const navigate = useNavigate()

	return (
		<React.Fragment>
			<HeaderTitle title="優惠活動" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['content']}>
					<div className={style['am-list']} onClick={() => navigate('/newsdetail')}>
						<div className={style['am-list-body']}>
							<div className={style['bodycon']}>
								<img className={style['banimg']} src={banner1} alt="" />
							</div>
						</div>
					</div>
					<div className={style['am-list']}>
						<div className={style['am-list-body']}>
							<div className={style['bodycon']}>
								<img className={style['banimg']} src={banner2} alt="" />
							</div>
						</div>
					</div>
					<div className={style['am-list']}>
						<div className={style['am-list-body']}>
							<div className={style['bodycon']}>
								<img className={style['banimg']} src={banner3} alt="" />
							</div>
						</div>
					</div>
					<div className={style['am-list']}>
						<div className={style['am-list-body']}>
							<div className={style['bodycon']}>
								<img className={style['banimg']} src={banner4} alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}


export default Promotionlist
