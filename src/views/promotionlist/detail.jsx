

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import banner1 from "../../static/1.jpg"
import banner5 from "../../static/5.jpg"

const Newsdetail = (props) => {
	const navigate = useNavigate()

	return (
		<React.Fragment>
			<HeaderTitle title="動 態" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['content']}>
					<div className={style['newsBox']}>
						<img className={style['banimg']} src={banner1} alt="" />
						<img className={style['banimg']} src={banner5} alt="" />
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}


export default Newsdetail
