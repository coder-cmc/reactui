

import { Link, useNavigate } from "react-router-dom";
import style from "./index.module.less"
import Tabbar from "./tabbar";
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { register } from "@/apis/user.js"

import messageicon from '../../static/icon-message.png'

import { message } from "antd";


const Mine = (props) => {
	const navigate = useNavigate()

	return (
		<React.Fragment>
			<div className={style['mineheader']}>
				<div style={{ width: '20px' }}></div>
				<div className={style['mhtitle']}>我的</div>
				<img className={style['mhimg']} src={messageicon} alt="" onClick={() => navigate('/dongtai')} />
			</div>
			<div className={style['minecon']}>
				<div className={style['mcheader']}>
					<img className={style['avatar']} src="https://img.ecartelera.com/noticias/60900/60941-m.jpg?v=2.0" alt="" />
					<div className={style['account-info']}>
						<div className={style['nick-name']}>df1122</div>
						<div className={style['info-item']}>
							<div className={style['label']}>餘額:</div>
							<div className={style['value']}>0.14</div>
						</div>
						<div className={style['info-item']}>
							<div className={style['label']}>最近提款金額:</div>
							<div className={style['value']}>0.00</div>
						</div>
					</div>
					<div className={style['setting']} onClick={() => navigate('/modify_password')}></div>
				</div>
				
				<div className={style['promote']} onClick={() => navigate('/discount_application')}>
					<div className={style['promoteicon']}></div>
					<div className={style['label']}>
						優惠中心
						<p>TASK CENTER</p>
					</div>
				</div>
				
				<div className={style['functional-buttons']}>
					<div className={style['buttons']} onClick={() => navigate('/money_bao')}>
						<div className={style['icon-transfer']}></div>
						<div className={style['label']}>我的錢包</div>
					</div>
					<div className={style['buttons']} onClick={() => navigate('/deposit')}>
						<div className={style['icon-deposit']}></div>
						<div className={style['label']}>存款</div>
					</div>
					<div className={style['buttons']} onClick={() => navigate('/withdraw')}>
						<div className={style['icon-withdraw']}></div>
						<div className={style['label']}>提款</div>
					</div>
				</div>
				
				<div className={style['list']}>
					<div className={style['menu-block']}>
						<div className={style['menu-item']} onClick={() => navigate('/invite')}>
							<div className={style['history']}></div>
							<div className={style['text']}>立即推薦好友</div>
						</div>
						<div className={style['menu-item']} onClick={() => navigate('/agent')}>
							<div className={style['rebate']}></div>
							<div className={style['text']}>全民代理佣金領取</div>
						</div>
						<div className={style['menu-item']} onClick={() => navigate('/profitloss')}>
							<div className={style['profit']}></div>
							<div className={style['text']}>今日盈虧</div>
						</div>
						<div className={style['menu-item']} onClick={() => navigate('/rechargerecord')}>
							<div className={style['history']}></div>
							<div className={style['text']}>充提記錄</div>
						</div>
						<div className={style['menu-item']} onClick={() => navigate('/account')}>
							<div className={style['statement']}></div>
							<div className={style['text']}>交易記錄</div>
						</div>
						<div className={style['menu-item']} onClick={() => navigate('/cardmanage')}>
							<div className={style['bankcard']}></div>
							<div className={style['text']}>銀行卡管理</div>
						</div>
					</div>
					<div className={style['menu-block']}>
						<div className={style['menu-item']} onClick={() => navigate('/myinfo')}>
							<div className={style['history']}></div>
							<div className={style['text']}>投注記錄</div>
						</div>
						<div className={style['menu-item']} onClick={() => navigate('/rakeback_get')}>
							<div className={style['rebate']}></div>
							<div className={style['text']}>返水領取</div>
						</div>
					</div>
				</div>
			</div>
			<Tabbar select="1"/>
		</React.Fragment>
	)
}


export default Mine
