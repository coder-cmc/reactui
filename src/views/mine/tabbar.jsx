import style from "./index.module.less"
import { Link, useNavigate } from "react-router-dom";
import React, { useState } from 'react';
import { Modal } from 'antd';

import logo from '../../static/logo.png'

const Tabbar = (props) => {
	const navigate = useNavigate()
	const [isModalOpen, setIsModalOpen] = useState(false);
	
	const showModal = () => {
		setIsModalOpen(true);
	};

	const handleCancel = () => {
		setIsModalOpen(false);
	};
	
	return (
		<div className={style['tabbar']}>
			<div className={style['tabbaritem']} style={{display: props.select != 0 ? 'flex' : 'none'}} onClick={() => navigate('/index')}>
				<div className={style['home']}></div>
				<div className={style['text']}>首頁</div>
			</div>
			<div className={style['tabbaritem']} style={{display: props.select == 0 ? 'flex' : 'none'}}>
				<div className={style['home1']}></div>
				<div className={style['text1']}>首頁</div>
			</div>
			<div className={style['tabbaritem']} onClick={() => navigate('/promotionlist')}>
				<div className={style['promote']}></div>
				<div className={style['text']}>優惠活動</div>
			</div>
			<div className={style['tabbaritem']}>
				<div className={style['download']}></div>
				<div className={style['text']}>APP下載</div>
			</div>
			<div className={style['tabbaritem']} onClick={showModal}>
				<div className={style['support']}></div>
				<div className={style['text']}>在線客服</div>
			</div>
			<div className={style['tabbaritem']} style={{display: props.select != 1 ? 'flex' : 'none'}} onClick={() => navigate('/mine')}>
				<div className={style['account']}></div>
				<div className={style['text']}>個人中心</div>
			</div>
			<div className={style['tabbaritem']} style={{display: props.select == 1 ? 'flex' : 'none'}}>
				<div className={style['account1']}></div>
				<div className={style['text1']}>個人中心</div>
			</div>
			<Modal open={isModalOpen} footer={[]} centered closeIcon={false}>
				<div className={style['modalcon']}>
					<div className={style['modalcontitle']}>線路選擇</div>
					<div className={style['modalconbody']}>
						<div className={style['service-item']}>
							<img className={style['logos']} src={logo} alt="" />
							<div className={style['texts']}>客服專員</div>
						</div>
					</div>
					<div className={style['close']} onClick={handleCancel}></div>
				</div>
			</Modal>
		</div>
	)
}

export default Tabbar