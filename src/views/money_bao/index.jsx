

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import money1 from '../../static/icon_transfer_top.png'
import sx from '../../static/icon-recycle.png'

const TIME = 60000; //定义时效性


const DongTai = (props) => {
	const navigate = useNavigate()
	const [active, serActive] = useState(1);
	const [visible, setVisible] = useState(false)
	const [formData, setFormData] = useState({ name: '', password: '', checked: false })
	const [form] = Form.useForm()
	// 查看缓存中是否有账号密码
	console.log(visible, '这个是啥---')
	console.log(form, '这个是啥---')
	let formDataMemo = {}
	if (localStorage.getItem('formData')) {
		formDataMemo = JSON.parse(localStorage.getItem('formData'))
	}

	if (formDataMemo.name && formDataMemo.password && formDataMemo.expirationTime && Date.now() < formDataMemo.expirationTime) {
		setFormData({
			name: formDataMemo.name,
			password: formDataMemo.password,
			checked: true,
		})

		console.log(formData, 'formData')
	}

	const onFinish = (values) => {
		console.log(values, 'values')
		/*
			1.调用登录接口
			2.登录成功之后，看其是否勾选了记住密码，如果记住密码则将账号密码缓存在内存中
			3.下次进入秘密
		*/
		login(values).then(res => {
			if (res.code == 200) {
				Toast.show({
					icon: 'success',
					content: "操作成功",
				})

				// 如果设置了记住密码，则在登录后将账号密码存储在cookie或storage中，并设置时效性以此来记住密码
				if (formData.checked) {
					localStorage.setItem('formData', JSON.stringify({
						...values,
						expirationTime: Date.now() + TIME,
					}));
				} else {
					localStorage.removeItem('formData')
				}
			} else {
				Toast.show({
					icon: 'fail',
					content: "操作失败",
				})

			}
		})

	};

	const onFinishFailed = (errorInfo) => {
		console.log('Failed:', errorInfo);
	};
	const changeTab = (current) => {
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}

	const items = [
		{ id:1,label: 'cq9电子', amount: '0.00' },
		{ id:2,label: '真人', amount: '7.00' },
		{ id:3,label: '页游生生世世', amount: '5.00' },
		// Add more items as needed
	  ];



	return (
		<React.Fragment>
			<HeaderTitle title="我的钱包" right_title="首页" />
			<div className={style['login_box']}>
				<div className={style['bg_class']}>
					<div className={style['flex']}>
						<div className={style['bg_content']}>
							<img src={money1} alt="" style={{ width: '50px' }} />
							<div style={{ color: '#fff' }}>
								<div className={style['flex']} style={{ marginTop: '10px' }}><div style={{ width: '80px', paddingLeft: '10px', fontSize: '10px' }}>账户</div> <div style={{ color: "#fbc847", marginLeft: "10px" }}>babayaa8</div></div>
								<div className={style['flex']} style={{ marginTop: '10px' }}><div style={{ width: '80px', paddingLeft: '10px', fontSize: '10px' }}>总余额</div><div style={{ marginLeft: "10px" }}>0</div></div>
							</div>
						</div>
					</div>
				</div>

				<div className={`${style['tabs']}`} onClick={() => {
					changeTab(1)
				}}>
					一键回收
				</div>

				<div className={style['flex']} style={{margin:'10px'}}>
					<div className={style['flex']}>
						<div className={style['left_label']}></div>
						<div style={{marginLeft:'10px',letterSpacing:'2px',color:'#841616'}}>提醒</div>
					</div>
					<div className={style['flex']} style={{marginLeft:'20px',fontSize:'12px',color:'#9d7201',paddingTop:'3px'}}>
						进入游戏会自动转入余额
					</div>
				</div>
				{items.map((item, index) => (
				<div className={style['item']} key={item.id} style={{backgroundColor:(index+1)%2==0?'#EDEDED':""}}>
					<div yle={{fontSize:'12px'}}>
					{item.label}
					</div>
					<div className={style['flex']}>
						<div className={style['flex']} style={{marginRight:'20px'}}>
							<span style={{color:"#9d7201",fontSize:'12px',fontWeight:'600',marginRight:'10px'}}>{item.amount}</span>
							<img src={sx} alt="" style={{ width: '15px',marginRight:'10px'}} />
						</div>
						<div className={style['money_button']}>转回主钱包</div>
					</div>
				</div>
				 ))}
			</div>


		</React.Fragment>


	)
}


export default DongTai
