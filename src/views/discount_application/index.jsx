

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import bg from '../../static/PageAnnouncement_bg_shh.png'
import gg from '../../static/notice_icon_shh.png'
import no_data from '../../static/icon_noData_bg.png'

const TIME = 60000; //定义时效性


const DongTai = (props) => {
	const navigate = useNavigate()
	const [active, serActive] = useState(1);
	const [visible, setVisible] = useState(false)
	const [formData, setFormData] = useState({ name: '', password: '', checked: false })
	const [form] = Form.useForm()
	// 查看缓存中是否有账号密码
	console.log(visible, '这个是啥---')
	console.log(form, '这个是啥---')
	let formDataMemo = {}
	if (localStorage.getItem('formData')) {
		formDataMemo = JSON.parse(localStorage.getItem('formData'))
	}

	if (formDataMemo.name && formDataMemo.password && formDataMemo.expirationTime && Date.now() < formDataMemo.expirationTime) {
		setFormData({
			name: formDataMemo.name,
			password: formDataMemo.password,
			checked: true,
		})

		console.log(formData, 'formData')
	}

	const onFinishFailed = (errorInfo) => {
		console.log('Failed:', errorInfo);
	};
	const changeTab = (current) => {
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}



	return (
		<React.Fragment>
			<HeaderTitle title="優惠申請" right_title="首頁" />
			<div className={style['login_box']}>

				<div className={style['operation2']}>
					{/* {[style['tabs'], style['active']].join(' ')} */}

					<div className={`${style['tabs']} ${active === 1 ? style['active'] : ''}`} onClick={() => {
						changeTab(1)
					}}>
						{/* <img src={gg} alt="" /> */}
						存款優惠申請
					</div>
					<div className={`${style['tabs']} ${active === 2 ? style['active'] : ''}`} onClick={() => {
						changeTab(2)
					}}>
						{/* <img src={xx} alt="" /> */}
						優惠申請記錄
					</div>
				</div>
				<div className={style['no_datas']}>
					<img src={no_data} alt="" style={{ width: '50px' }} />
					<div style={{ color: '#CACBCE', marginTop: '10px' }}>沒有更多記錄</div>
				</div>

				{/* <div className={style['content']}>
					<div className={[style['operation']].join(' ')}>
						<span>
							優惠申請記錄-----
						</span>
						<span onClick={() => navigate('/emptyPage')} style={{ color: '#999' }}>2023/11/06</span>
					</div>
				</div> */}

			</div>



		</React.Fragment>


	)
}


export default DongTai
