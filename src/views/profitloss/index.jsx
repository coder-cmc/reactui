

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import React, { Fragment, useState } from 'react'
import { Select } from 'antd'
import { login } from "@/apis/user.js"


const Profitlist = (props) => {
	const navigate = useNavigate()
	const [selecttime,setselecttime]=useState(0)

	const changeTab=(current)=>{
		setselecttime(current) //更新 active 状态
	}
	
	const handleChange=(value)=>{
		console.log(value)
	}

	return (
		<React.Fragment>
			<HeaderTitle title="今日盈虧" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['header_group']}>
					<div className={style['time_group']}>
						<div className={`${style['tgitem']} ${selecttime === 0 ? style['active'] : ''}`} onClick={()=>{changeTab(0)}}>今天</div>
						<div className={`${style['tgitem']} ${selecttime === 1 ? style['active'] : ''}`} onClick={()=>{changeTab(1)}}>昨天</div>
						<div className={`${style['tgitem']} ${selecttime === 2 ? style['active'] : ''}`} onClick={()=>{changeTab(2)}}>7天</div>
						<div className={`${style['tgitem']} ${selecttime === 3 ? style['active'] : ''}`} onClick={()=>{changeTab(3)}}>30天</div>
					</div>
					<div className="game_group">
						<Select
						  defaultValue="總計"
						  style={{ width: '100%',height: '34px' }}
						  onChange={handleChange}
						  options={[
							{ value: '1', label: 'CQ9電子' },
							{ value: '2', label: 'Motivation Gaming真人' },
							{ value: '3', label: '樂遊' },
							{ value: '4', label: '開元棋牌' },
						  ]}
						/>
					</div>
				</div>
				<div className={style['content_group']}>
					<div className={style['main_content']}>
						<div className={style['header_group_bg']}>
							<div className={style['header_group_content']}>
								<div className={style['total_profit_loss']}>
									<span>盈虧總額</span>
									<span className={style['green']}>0.00</span>
								</div>
								<div className={style['avatar']}>
									<div className={style['avatar_bg']}></div>
								</div>
								<div className={style['total_profit_loss']}>
									<span>帳戶餘額</span>
									<span>0.14</span>
								</div>
							</div>
							<div className={style['txt_group']}>盈虧總計=派獎金額-投注金額+活動禮金+所有第三方盈利</div>
						</div>
						<div className={style['Recharge_group']}>
							<div className={style['rechargeBox']}>
								<div className={style['rechargeBoxitem']} onClick={() => navigate('/deposit')}>
									<div className={style['recharge_icon']}></div>
									<div>存款</div>
								</div>
								<div className={style['rechargeBoxitem']} onClick={() => navigate('/withdraw')}>
									<div className={style['withdraw_icon']}></div>
									<div>提款</div>
								</div>
							</div>
						</div>
						<div className={style['txt_recharge_group']}>
							<div className={style['rechargegroupitem']}>
								<span>已充值</span>
								<span className={style['txt_coin']}>0.00</span>
							</div>
							<div className={style['rechargegroupitem']}>
								<span>已提款</span>
								<span className={style['txt_coin']}>0.00</span>
							</div>
						</div>
						<div className={style['icon_group']}>
							<div className={style['DataGroup']}>
								<div className={style['DataGroupItem']}>
									<span>投注金額</span>
									<span className={style['txt_coin']}>0.00</span>
								</div>
								<div className={style['DataGroupItem']}>
									<span>中獎金額</span>
									<span className={style['txt_coin']}>0.00</span>
								</div>
								<div className={style['DataGroupItem']}>
									<span>活動禮金</span>
									<span className={style['txt_coin']}>0.00</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}


export default Profitlist
