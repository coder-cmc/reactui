

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { register } from "@/apis/user.js"

import logo from '../../static/logo.png'

const Register = (props) => {
	const navigate = useNavigate()
	const [visible, setVisible] = useState(false)
	const [formData, setFormData] = useState({ name: '', password: '', username: '', phonenumber: '', yzm: '', comfirmpassword: '' })
	const [form] = Form.useForm()

	const onFinish = (values) => {
		console.log(values, 'values')
		/*
			1.调用登录接口
			2.登录成功之后，看其是否勾选了记住密码，如果记住密码则将账号密码缓存在内存中
			3.下次进入秘密
		*/
		register(values).then(res => {
			if (res.code == 200) {
				Toast.show({
					icon: 'success',
					content: "操作成功",
				})

			} else {
				Toast.show({
					icon: 'fail',
					content: "操作失败",
				})

			}
		})

	};

	const onFinishFailed = (errorInfo) => {
		console.log('Failed:', errorInfo);
	};



	return (
		<React.Fragment>
			<HeaderTitle title="注 册" right_title="首页"/>
			<div className={style['login_box']}>
				<div style={{ textAlign: 'center' }}>
					<img src={logo} alt="" />
				</div>
				<Form
					form={form}
					layout='horizontal'
					onFinish={onFinish}
					initialValues={formData}
				>
					<Form.Item
						label='账 号'
						name='name'
						rules={[{ required: true, message: '账号不能为空!' }, { pattern: /^(?=\w*\d)(?=\w*[a-zA-Z])[\w]{6,12}$/, message: '请输入6-12位数字和字母组合' }]}
					>
						<Input placeholder='6-12个英文字母和数字' />
					</Form.Item>
					<Form.Item
						label='真实姓名'
						name='username'
						rules={[{ required: true, message: '真实姓名不能为空!' }]}
					>
						<Input placeholder='请输入真实姓名' />
					</Form.Item>
					<Form.Item
						label='手机号码'
						name='phonenumber'
						rules={[{ required: true, message: '手机号码不能为空!' }]}
					>
						<Input placeholder='请输入手机号码' />
					</Form.Item>
					<Form.Item
						label='验证码'
						name='yzm'
						rules={[{ required: true, message: '验证码不能为空!' }]}
						extra={
							<div style={{ color: '#e38a00' }}>发送验证码</div>
						}
					>
						<Input placeholder='请输入验证码' />
					</Form.Item>
					<Form.Item
						label='密 码'
						name='password'
						rules={[{ required: true, message: '密码不能为空!' }]}
						extra={
							<div>
								{!visible ? (
									<EyeInvisibleOutline onClick={() => setVisible(true)} />
								) : (
									<EyeOutline onClick={() => setVisible(false)} />
								)}
							</div>
						}>
						<Input placeholder='请输入密码' type={visible ? 'text' : 'password'} />
					</Form.Item>
					<Form.Item
						label='确认密码'
						name='comfirmpassword'
						rules={[{ required: true, message: '确认密码不能为空!' },{ pattern: /^(?=\w*\d)(?=\w*[a-zA-Z])[\w]{6,16}$/, message: '请输入6-16位字符组合'}]}
						extra={
							<div>
								{!visible ? (
									<EyeInvisibleOutline onClick={() => setVisible(true)} />
								) : (
									<EyeOutline onClick={() => setVisible(false)} />
								)}
							</div>
						}>
						<Input placeholder='请输入6-16位字符组合' type={visible ? 'text' : 'password'} />
					</Form.Item>
				</Form>
				<React.Fragment>
					<Button shape='rounded' className={style['btns']} style={{ marginBottom: '15px', marginTop: '15px' }} block onClick={() => {
						console.log(form, 'fomr')
						form.validateFields()
						form.submit()
					}} color='primary'>
						注册
					</Button>
					<p className={style['other-info']} onClick={() => navigate('/login')}>已有 Game One 账号</p>

				</React.Fragment>
			</div>
		</React.Fragment>

	)
}


export default Register
