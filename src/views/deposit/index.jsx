

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import zsk from '../../static/icon-quickly-pay.png'
import usdt from '../../static/icon-usdt-shield.png'
import ms from '../../static/icon-cvs-pay.png'
import right from '../../static/right.png'

const TIME = 60000; //定义时效性


const DongTai = (props) => {
	const navigate = useNavigate()
	const [active, serActive] = useState(1);
	const [visible, setVisible] = useState(false)
	const [formData, setFormData] = useState({ name: '', password: '', checked: false })
	const [form] = Form.useForm()
	// 查看缓存中是否有账号密码
	console.log(visible, '这个是啥---')
	console.log(form, '这个是啥---')
	let formDataMemo = {}

	const changeTab = (current) => {
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}

	const items = [
		{ id: 1, label: '轉數快-b', amount: '0.00' },
		{ id: 2, label: '轉數快-a', amount: '7.00' },
		{ id: 3, label: '轉數快-c', amount: '5.00' },
		// Add more items as needed
	];



	return (
		<React.Fragment>
			<HeaderTitle title="存款" right_title="首页" />
			<div className={style['login_box']}>

				{/* <div className={`${style['tabs']}`} onClick={() => {
					changeTab(1)
				}}>
					一键回收
				</div> */}

				<div className={style['flex']} style={{ margin: '10px' }}>
					<div className={style['flex']}>
						<div className={style['left_label']}></div>
						<div style={{ marginLeft: '10px', letterSpacing: '2px', color: '#841616' }}>轉數快</div>
					</div>
					<div className={style['flex']} style={{ marginLeft: '20px', fontSize: '12px', color: '#9d7201', paddingTop: '3px' }}>

					</div>
				</div>

				{items.map((item, index) => (
					<div className={style['item']} key={item.id} onClick={() => navigate('/rechargeorder')}>
						<div className={style['flex']}>
							<div className={style['flex']} style={{ marginRight: '20px' }}>
								<img src={zsk} alt="" style={{ width: '30px', marginRight: '10px' }} />
							</div>
							<div className>{item.label}</div>
						</div>

						<div yle={{ fontSize: '12px' }}>
							<img src={right} alt="" style={{ width: '20px', marginRight: '10px' }} />
						</div>

					</div>
				))}

				<div className={style['flex']} style={{ margin: '10px' }}>
					<div className={style['flex']}>
						<div className={style['left_label']}></div>
						<div style={{ marginLeft: '10px', letterSpacing: '2px', color: '#841616' }}>門市付款</div>
					</div>
					<div className={style['flex']} style={{ marginLeft: '20px', fontSize: '12px', color: '#9d7201', paddingTop: '3px' }}>

					</div>
				</div>
				{items.map((item, index) => (
					<div className={style['item']} key={item.id}>
						<div className={style['flex']}>
							<div className={style['flex']} style={{ marginRight: '20px' }}>
								<img src={ms} alt="" style={{ width: '30px', marginRight: '10px' }} />
							</div>
							<div className>門市付款</div>
						</div>

						<div yle={{ fontSize: '12px' }}>
							<img src={right} alt="" style={{ width: '20px', marginRight: '10px' }} />
						</div>

					</div>
				))}

				<div className={style['flex']} style={{ margin: '10px' }}>
					<div className={style['flex']}>
						<div className={style['left_label']}></div>
						<div style={{ marginLeft: '10px', letterSpacing: '2px', color: '#841616' }}>加密貨幣充值</div>
					</div>

				</div>
				<div className={style['red']}>
					<div style={{  }}>
						熱門推薦
					</div>
					<div style={{marginTop:'5px'}}>
						使用加密貨幣充值，大額娛樂首選！
					</div>

				</div>
				<div className={style['flex']} style={{ margin: '10px' }}>
					<div className={style['flex']}>
						<div className={style['left_label']}></div>
						<div style={{ marginLeft: '10px', letterSpacing: '2px', color: '#841616' }}>門市付款</div>
					</div>
					<div className={style['flex']} style={{ marginLeft: '20px', fontSize: '12px', color: '#9d7201', paddingTop: '3px' }}>

					</div>
				</div>
				{items.map((item, index) => (
					<div className={style['item']} key={item.id}>
						<div className={style['flex']}>
							<div className={style['flex']} style={{ marginRight: '20px' }}>
								<img src={usdt} alt="" style={{ width: '30px', marginRight: '10px' }} />
							</div>
							<div className>USDT</div>
						</div>
						<div style={{ fontSize: '16px',display:'flex',alignItems:'center'}}>
							<span style={{marginRight:'10px'}}>TRC20充值</span>
							<img src={right} alt="" style={{ width: '20px', marginRight: '10px' }} />
						</div>

					</div>
				))}












			</div>


		</React.Fragment>


	)
}


export default DongTai
