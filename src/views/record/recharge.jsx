

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./recharge.module.less"
import React, { Fragment, useState } from 'react'
import { Select, DatePicker } from 'antd'
import dayjs from 'dayjs'
import { login } from "@/apis/user.js"


const Recharge = (props) => {
	const navigate = useNavigate()
	const [selecttime,setselecttime]=useState(0)
	const [starttime,setstarttime]=useState('2023-11-09')
	const [endtime,setendtime]=useState('2023-11-09')
	const dateFormat = 'YYYY-MM-DD';
	
	const changeTab=(current)=>{
		setselecttime(current) //更新 active 状态
	}
	
	const handleChange=(value)=>{
		console.log(value)
	}
	
	function onChange(date, dateString) {
		setstarttime(dateString)
	}
	
	function onChange1(date, dateString) {
		setendtime(dateString)
	}

	return (
		<React.Fragment>
			<HeaderTitle title="充提記錄" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['header_group']}>
					<div className={style['time_group']}>
						<div className={`${style['tgitem']} ${selecttime === 0 ? style['active'] : ''}`} onClick={()=>{changeTab(0)}}>今天</div>
						<div className={`${style['tgitem']} ${selecttime === 1 ? style['active'] : ''}`} onClick={()=>{changeTab(1)}}>昨天</div>
						<div className={`${style['tgitem']} ${selecttime === 2 ? style['active'] : ''}`} onClick={()=>{changeTab(2)}}>7天</div>
						<div className={`${style['tgitem']} ${selecttime === 3 ? style['active'] : ''}`} onClick={()=>{changeTab(3)}}>30天</div>
					</div>
					<div className={style['date-section']}>
						<DatePicker onChange={onChange} defaultValue={dayjs(starttime, dateFormat)}/>
						<DatePicker onChange={onChange1} defaultValue={dayjs(endtime, dateFormat)} />
						<div className={style['date-search']}>查詢</div>
					</div>
					<div className="game_group">
						<Select
						  defaultValue="全部"
						  style={{ width: '100%',height: '34px' }}
						  onChange={handleChange}
						  options={[
							{ value: '1', label: '全部' },
							{ value: '2', label: '存款' },
							{ value: '3', label: '提款' },
						  ]}
						/>
					</div>
				</div>
				<div className={style['content']}>
					<div className={style['table']}>
						<div className={style['thead']}>
							<div className={style['tr']}>
								<div className={style['td']}>日期</div>
								<div className={style['td']}>內容</div>
								<div className={style['td']}>金額</div>
								<div className={style['td']}>狀態</div>
							</div>
						</div>
						<div className={style['tbody']}>
							<div className={style['tr']}>
								<div className={style['td']}>2023-11-09 16:55:01</div>
								<div className={style['td']}>存款</div>
								<div className={style['td']}>100.24</div>
								<div className={style['td']}>申請中</div>
							</div>
						</div>
					</div>
					<div className={style['NoDataGroup']}>
						<div className={style['noData_bg']}></div>
						<p className={style['txt_tit']}>沒有更多記錄</p>
					</div>
				</div>
			</div>
			<div className={style['']}></div>
		</React.Fragment>
	)
}


export default Recharge
