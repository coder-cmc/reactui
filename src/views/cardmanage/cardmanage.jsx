

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import React, { Fragment, useState } from 'react'
import { Modal, Input } from 'antd';

import { login } from "@/apis/user.js"


const Cardmanage = (props) => {
	const navigate = useNavigate()
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [selecttab,setselecttab]=useState(0)
	
	const showModal = () => {
		setIsModalOpen(true);
	};
	
	const handleCancel = () => {
		setIsModalOpen(false);
	};
	
	const changeTab=(current)=>{
		setselecttab(current) //更新 active 状态
	}
	
	return (
		<React.Fragment>
			<HeaderTitle title="銀行卡管理" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['header_group']}>
					<div className={style['TitGroup']}>
						<div className={style['iconBox']}>
							<div className={style['icon_img']}></div>
						</div>
						<div className={style['txt_tit']}>點擊銀行卡，可更改預設設定</div>
					</div>
				</div>
				<div className={style['content_group']}>
					<div className={style['main_content']}>
						<div className={style['icon_group']}>
							<div className={style['data_group']}>
								<div className={style['data_group_box']}>
									<div className={style['data_group_box_item']}>
										<div className={style['txt_title']}>轉數快</div>
										<div className={style['txt_content']}>
											<p>
												<span className={style['txt_no']}>1123**********1123</span>
												<span className={style['txt_account']}>GUTIANLE</span>
											</p>
										</div>
										{/* 选中的图片加在下面div的背景图片 样式已经写好了 */}
										<div className={style['arrow']}></div>
									</div>
									<div className={style['data_group_box_item']}>
										<div className={style['txt_title']}>003 渣打銀行(香港)有限公司</div>
										<div className={style['txt_content']}>
											<p>
												<span className={style['txt_no']}>2323**********2323</span>
												<span className={style['txt_account']}>GUTIANLE</span>
											</p>
										</div>
										<div className={style['arrow']}></div>
									</div>
								</div>
								<div className={style['AddCardGroup']}>
									<div className={style['addCard_bg']} onClick={showModal}></div>
									<div className={style['txt_tit']} onClick={showModal}>
										<span className={style['hit-text']}>[</span>
										<span className={style['hit-text']}>點擊 </span>
										<span>新增銀行卡</span>
										<span className={style['hit-text']}>]</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Modal open={isModalOpen} footer={[]} centered closeIcon={false}>
				<div className={style['modalcon']}>
					<div className={style['modalcontitle']}>綁定銀行帳戶</div>
					<div className={style['close']} onClick={handleCancel}></div>
					<div className={style['modalconbody']}>
						<div className={style['tabs']}>
							<div className={`${style['tabsitem']} ${selecttab === 0 ? style['active'] : ''}`} onClick={()=>{changeTab(0)}}>USDT-TRC20</div>
							<div className={`${style['tabsitem']} ${selecttab === 1 ? style['active'] : ''}`} onClick={()=>{changeTab(1)}}>USDT-ERC20</div>
						</div>
						<div className={style['form_group']}>
							<div className={style['rate']}>
								當前匯率
								<span style={{ color: 'red' }}>1 USDT: 7.87</span>
							</div>
							<div className={style['payName']}>
								<span>真實姓名: GUTIANLE</span>
							</div>
							<Input placeholder="請輸入您的TRC20收款地址" />
							<div className={style['BtnSubmit']}>
								<div className={style['btns']}>提交</div>
							</div>
						</div>
					</div>
				</div>
			</Modal>
		</React.Fragment>
	)
}


export default Cardmanage
