import style from "./index.module.less"
const NewsTicker = ({ newsItems }) => (
    <ul className="news-ticker">
            {newsItems.map((item, index) => (
                <li key={index}>{item}</li>
            ))}
    </ul>
);

export default NewsTicker;