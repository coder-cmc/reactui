

import { Link, useNavigate } from "react-router-dom";
import style from "./index.module.less"
import Tabbar from "../mine/tabbar";
import NewsTicker from './new';
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import { Carousel } from 'antd';
import { register } from "@/apis/user.js"

import logo from '../../static/logo.png'
import avatar from '../../static/icon-account.png'
import HK from '../../static/hk.png'
import EN from '../../static/en.png'
import TH from '../../static/th.png'
import DB4 from '../../static/db4.jpg'
import A515 from '../../static/515.jpg'
import DRAGONBG from '../../static/dragon-bg.png'
import Logodg from '../../static/logo-dg-blue.png'
import Bkdg from '../../static/bk-dg.png'

import { message } from "antd";

const Mine = (props) => {
	const navigate = useNavigate()
	const [lang,setLang]=useState(false);
	const [langs,setLangs]=useState(0);
	const [active,serActive]=useState(1);
	const [actives,serActives]=useState(true);
	const newsItems=['《遊戲廠商狀態更新公告》 尊敬的GAMEONE會員！<開元棋牌>和<樂遊棋牌> 這兩家遊戲廠商將於 11月1日 起屏蔽香港IP，因此香港玩家需要使用VPN轉移至其他地區線路方能進入遊戲，造成閣下的不便我們深感抱歉！']
	
	const changeLang=()=>{
		let status = !lang
		setLang(status) //更新 active 状态
	}
	const changeLangs=(current)=>{
		setLangs(current) //更新 active 状态
		changeLang()
	}
	
	const changeTab=(current)=>{
		serActives(false)
		serActive(current) //更新 active 状态
		setTimeout(function(){
			serActives(true)
		},500)
	}
	
	const contentStyle=React.CSSProperties = {
		margin: 0,
		height: '40.774vmin',
		color: '#fff',
		lineHeight: '160px',
		textAlign: 'center',
		background: '#364d79',
		borderRadius: '10px',
	}
	const onChange = (currentSlide=number) => {
	    console.log(currentSlide)
	}
	const onChange1 = (currentSlide=number) => {
	    console.log(currentSlide)
	}
	
	return (
		<React.Fragment>
			<div className={style['home']}>
				<div className={style['sportIIHeader']}>
					<div className={style['logo']}>
						<img src={logo} alt="" />
					</div>
					<div className={style['functional']}>
						<div className={style['account-info']}>
							<img src={avatar} alt="" />
							<span className={style['userid']}>df1122</span>
						</div>
						<div className={style['func-btn']}>
							<div className={style['icon-logout']}></div>
							<div className={style['label']}>登出</div>
						</div>
						<div className={style['func-btn']}>
							<div className={style['icon-sound-on']}></div>
						</div>
						<div className={style['lang-wrapper']}>
							<div className={style['current-lang']} onClick={()=>{changeLang()}}>
								{
									langs == 0 ? (<img src={HK} alt="" />) : ''
								}
								{
									langs == 1 ? (<img src={EN} alt="" />) : ''
								}
								{
									langs == 2 ? (<img src={TH} alt="" />) : ''
								}
							</div>
							{
								lang ? (<div className={style['select-langs']}>
									<div className={style['option-lang']} onClick={()=>{changeLangs(0)}}>
										<img src={HK} alt="" />
									</div>
									<div className={style['option-lang']} onClick={()=>{changeLangs(1)}}>
										<img src={EN} alt="" />
									</div>
									<div className={style['option-lang']} onClick={()=>{changeLangs(2)}}>
										<img src={TH} alt="" />
									</div>
								</div>) : ''
							}
						</div>
					</div>
				</div>
				<Carousel afterChange={onChange} autoplay>
				    <div>
				        <img src={DB4} alt="" />
				    </div>
				    <div>
				        <img src={A515} alt="" />
				    </div>
				</Carousel>
				<div className={style['notice']}>
					<div className={style['noticeicon']}></div>
					<NewsTicker newsItems={newsItems}/>
				</div>
				<div className={style['dragon-tiger-container']}>
					<img src={DRAGONBG} alt="" />
					<div className={style['dragon-tiger-content']}>
						<Carousel dotPosition='right' afterChange={onChange1} autoplay dots={false}>
							<div>
								<h3 className={style['items']}>
									<span>204***122</span>
									<span>$225154</span>
								</h3>
							</div>
							<div>
								<h3 className={style['items']}>
									<span>204***122</span>
									<span>$225154</span>
								</h3>
							</div>
							<div>
								<h3 className={style['items']}>
									<span>204***122</span>
									<span>$225154</span>
								</h3>
							</div>
							<div>
								<h3 className={style['items']}>
									<span>204***122</span>
									<span>$225154</span>
								</h3>
							</div>
						</Carousel>
					</div>
				</div>
				<div className={style['accountFunctions']}>
					<div className={style['account-user']}>
						<div className={style['online-number']}>
							<div>在線人數</div>
							<div>73529</div>
						</div>
						<div className={style['balances']}>
							<div className={style['balance']}>
								<div className={style['label']}>點數</div>
								<div>0.14</div>
							</div>
							<div className={style['balance']}>
								<div className={style['label']}>鎖定點數</div>
								<div>0.00</div>
							</div>
						</div>
					</div>
					<div className={style['buttons']}>
						<div className={style['account-button']}>
							<div className={style['icon-gift']}></div>
							<div className={style['label']}>禮物兌換</div>
						</div>
						<div className={style['account-button']}>
							<div className={style['icon-rebate']}></div>
							<div className={style['label']}>返水領取</div>
						</div>
						<div className={style['account-button']}>
							<div className={style['icon-deposit']}></div>
							<div className={style['label']}>存款</div>
						</div>
						<div className={style['account-button']}>
							<div className={style['icon-withdraw']}></div>
							<div className={style['label']}>提款</div>
						</div>
					</div>
				</div>
				<div className={style['homeSportIIGameCategory']}>
					<div className={style['game-category-content']}>
						<div className={style['game-category']}>
							<div className={`${style['category-filter']} ${active === 1 ? style['active'] : ''}`} onClick={()=>{changeTab(1)}}>
								<div className={style['icon-live']}></div>
								<div className={style['label']}>真人</div>
							</div>
							<div className={`${style['category-filter']} ${active === 2 ? style['active'] : ''}`} onClick={()=>{changeTab(2)}}>
								<div className={style['icon-sport']}></div>
								<div className={style['label']}>體育</div>
							</div>
							<div className={`${style['category-filter']} ${active === 3 ? style['active'] : ''}`} onClick={()=>{changeTab(3)}}>
								<div className={style['icon-zone']}></div>
								<div className={style['label']}>專區</div>
							</div>
						</div>
						<div className={`${style['game-list']} ${actives ? style['active'] : ''}`}>
							<div className={style['enter-game-button']}>
								<div className={style['game-name']}>
									<div>
										<img src={Logodg} alt="" />
										<div className={style['label']}>DG真人</div>
									</div>
								</div>
								<img className={style['img']} src={Bkdg} alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className={style['']}></div>
			<Tabbar select="0"/>
		</React.Fragment>
	)
}


export default Mine
