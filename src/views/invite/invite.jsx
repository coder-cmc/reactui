

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import React, { Fragment, useState } from 'react'
import { Input, QRCode, Space, message } from 'antd';

import { login } from "@/apis/user.js"

import banner1 from "../../static/1.jpg"
import banner2 from "../../static/2.jpg"
import banner3 from "../../static/3.jpg"
import banner4 from "../../static/4.jpg"


const Invite = (props) => {
	const navigate = useNavigate()
	const [text, setText] = React.useState('http://g1vip.com?p=116072');
	
	const copy = () => {
		//因为我的input框里面还有button 按钮，所以在选择节点的时候，一定要只选择input  
		var copyDOM = document.querySelector(".invite");  //需要复制文字的节点
		var range = document.createRange(); //创建一个range
		window.getSelection().removeAllRanges();   //清楚页面中已有的selection
		range.selectNode(copyDOM);    // 选中需要复制的节点    
		window.getSelection().addRange(range);   // 执行选中元素
		var successful = document.execCommand('copy');    // 执行 copy 操作  
		if(successful){
			message.success('复制成功！')
		}else{
			message.error('复制失败，请手动复制！')
		}
		// 移除选中的元素  
		window.getSelection().removeAllRanges(); 
	}
	
	return (
		<React.Fragment>
			<HeaderTitle title="立即推薦好友" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['tableheader']}>
					<div className={style['tableheader_li']}>總邀請好友</div>
					<div className={style['tableheader_li']}>時間</div>
					<div className={style['tableheader_li']}>好友有效投注額</div>
				</div>
				<div className={style['tablebady']}>
					<div className={style['tablebadyul']}>
						<div className={style['tablebadyli']}>
							<span>0</span>
							<span>2023-11-08</span>
							<span>0</span>
						</div>
						<div className={style['tablebadyli']}>
							<span>0</span>
							<span>2023-11-07</span>
							<span>0</span>
						</div>
					</div>
				</div>
				<Space direction="vertical" align="center">
				  <QRCode value={text || '-'} size="232" bgColor="#ffffff"/>
				</Space>
				<div className={style['detail']}>
					<h2>推薦鏈接:</h2>
					<div className="invite">{text}</div>
					<div className={style['btn']} onClick={copy}>複製</div>
				</div>
			</div>
		</React.Fragment>
	)
}


export default Invite
