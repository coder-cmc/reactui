

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import React, { Fragment, useState } from 'react'
import { Input, QRCode, Space, message } from 'antd';

import { login } from "@/apis/user.js"

const Invite = (props) => {
	const navigate = useNavigate()
	const [active,serActive]=useState(1);
	const [name,serName]=useState(null);
	const [zhuan,serZhuan]=useState(null);
	const [trc20,serTrc20]=useState(null);
	const [erc20,serErc20]=useState(null);
	
	const changeTab=(current)=>{
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}
	
	return (
		<React.Fragment>
			<HeaderTitle title="綁定銀行帳戶" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['withdraw_group']}>
					<div className={style['withdrawBox']}>
						<div className={`${style['wbli']} ${active === 1 ? style['active'] : ''}`} onClick={()=>{changeTab(1)}}>
							<div className={style['btn']}>轉數快提款</div>
						</div>
						<div className={`${style['wbli']} ${active === 2 ? style['active'] : ''}`} onClick={()=>{changeTab(2)}}>
							<div className={style['btn']}>銀行卡提款</div>
						</div>
						<div className={`${style['wbli']} ${active === 3 ? style['active'] : ''}`} onClick={()=>{changeTab(3)}}>
							<div className={style['btn']}>USDT-TRC20提款</div>
						</div>
						<div className={`${style['wbli']} ${active === 4 ? style['active'] : ''}`} onClick={()=>{changeTab(4)}}>
							<div className={style['btn']}>USDT-ERC20提款</div>
						</div>
					</div>
				</div>
				<div className={style['forms']}>
					{
						active == 1 ? (<div className={style['form-ul']}>
							<div className={style['fli']}>
								<label>真实姓名</label>
								<Input defaultValue={name} placeholder="请保证真实有效，否则会影响提现" />
							</div>
							<div className={style['fli']}>
								<label>轉數快号码</label>
								<Input defaultValue={zhuan} placeholder="请输入轉數快号码" />
							</div>
						</div>) : ''
					}
					{
						active == 3 ? (<div className={style['form-ul']}>
							<div className={style['fli']}>
								<label>提款匯率：</label>
								<span>1 USDT : 7.87</span>
							</div>
							<div className={style['fli']}>
								<label>真實姓名</label>
								<span style={{color: '#333'}}>GUTIANLE</span>
							</div>
							<div className={style['fli']}>
								<label>TRC20地址</label>
								<Input defaultValue={trc20} placeholder="請輸入您的USDT收款地址" />
							</div>
						</div>) : ''
					}
					{
						active == 4 ? (<div className={style['form-ul']}>
							<div className={style['fli']}>
								<label>提款匯率：</label>
								<span>1 USDT : 7.87</span>
							</div>
							<div className={style['fli']}>
								<label>真實姓名</label>
								<span style={{color: '#333'}}>GUTIANLE</span>
							</div>
							<div className={style['fli']}>
								<label>ERC20地址</label>
								<Input defaultValue={erc20} placeholder="請輸入您的USDT收款地址" />
							</div>
						</div>) : ''
					}
					<div className={style['operate']}>
						<div className={style['submitBtn']}>提交</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}


export default Invite
