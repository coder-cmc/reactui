import React from 'react';
import style from './TableHeaderContent1.module.less'; // Import the CSS module for styles

const TableHeaderContent1 = () => {
  return (
    <React.Fragment>
    <div className={style['tableheader']}>
      <div className={style['tableheader_li']}>活動</div>
      <div className={style['tableheader_li']}>昨日打嗎</div>
      <div className={style['tableheader_li']}>獲得返水</div>
      <div className={style['tableheader_li']}>領取</div>
    </div>
    	<div className={style['tablebady']}>
        <div className={style['tablebadyul']}>
          <div className={style['tablebadyli']}>
            <span>--</span>
            <span>2023-11-08</span>
            <span>2023-11-08</span>
            <span>領取</span>
          </div>
          <div className={style['tablebadyli']}>
            <span>0</span>
            <span>2023-11-07</span>
            <span>2023-11-07</span>
            <span>0</span>
          </div>
        </div>
    </div>
    </React.Fragment>
  );
};

export default TableHeaderContent1;