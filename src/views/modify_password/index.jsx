

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
    Form,
    Input,
    Checkbox,
    Button,
    Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import money1 from '../../static/icon_transfer_top.png'
import eye from '../../static/eye.png'
import eyeo from '../../static/eye-o.png'

const TIME = 60000; //定义时效性


const DongTai = (props) => {
    const navigate = useNavigate()
    const [active, serActive] = useState(1);
    const [visible, setVisible] = useState(false)
    const [visible1, setVisible1] = useState(false)
    const [visible2, setVisible2] = useState(false)
    const [formData, setFormData] = useState({ name: '', password: '', checked: false })
    const [form] = Form.useForm()
    // 查看缓存中是否有账号密码
    console.log(visible, '这个是啥---')
    console.log(form, '这个是啥---')
    let formDataMemo = {}
    if (localStorage.getItem('formData')) {
        formDataMemo = JSON.parse(localStorage.getItem('formData'))
    }

    if (formDataMemo.name && formDataMemo.password && formDataMemo.expirationTime && Date.now() < formDataMemo.expirationTime) {
        setFormData({
            name: formDataMemo.name,
            password: formDataMemo.password,
            checked: true,
        })

        console.log(formData, 'formData')
    }

    const onFinish = (values) => {
        console.log(values, 'values')
        /*
            1.调用登录接口
            2.登录成功之后，看其是否勾选了记住密码，如果记住密码则将账号密码缓存在内存中
            3.下次进入秘密
        */
        login(values).then(res => {
            if (res.code == 200) {
                Toast.show({
                    icon: 'success',
                    content: "操作成功",
                })

                // 如果设置了记住密码，则在登录后将账号密码存储在cookie或storage中，并设置时效性以此来记住密码
                if (formData.checked) {
                    localStorage.setItem('formData', JSON.stringify({
                        ...values,
                        expirationTime: Date.now() + TIME,
                    }));
                } else {
                    localStorage.removeItem('formData')
                }
            } else {
                Toast.show({
                    icon: 'fail',
                    content: "操作失败",
                })

            }
        })

    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const changeTab = (current) => {
        console.log('current:', current);
        serActive(current) //更新 active 状态
    }

    const items = [
        { id: 1, label: 'cq9电子', amount: '0.00' },
        { id: 2, label: '真人', amount: '7.00' },
        { id: 3, label: '页游生生世世', amount: '5.00' },
        // Add more items as needed
    ];



    return (
        <React.Fragment>
            <HeaderTitle title="修改密码" right_title="首页" />
            <div className={style['login_box']}>


                <div className={`${style['tabs']}`} onClick={() => {
                    changeTab(1)
                }} style={{ letterSpacing: '2px' }}>
                    修改登录密码
                </div>



                <div className={style['item-block']}>

                    <div className={style['item']}>
                        <div style={{ fontSize: '12px' }}>
                            <input type={visible?'text':'password'} placeholder="请输入旧密码" />
                        </div>
                        <div className={style['flex']}>
                            <div className={style['flex']}>
                                <img src={visible?eyeo:eye} onClick={() => {
                    setVisible(!visible)
                }} alt="" style={{ width: '30px', marginRight: '10px' }} />
                            </div>
                        </div>
                    </div>
                    <div className={style['item']}>
                        <div style={{ fontSize: '12px' }}>
                            <input type={visible1?'text':'password'} placeholder="请输入新密码，密码有=由6-16个英数组成" />
                        </div>
                        <div className={style['flex']}>
                            <div className={style['flex']}>
                                <img src={visible1?eyeo:eye} onClick={() => {
                    setVisible1(!visible1)
                }} alt="" style={{ width: '30px', marginRight: '10px' }} />
                            </div>
                        </div>
                    </div>
                    <div className={style['item']} style={{marginBottom:'20px'}}>
                        <div style={{ fontSize: '12px' }}>
                            <input type={visible2?'text':'password'} placeholder="请确认新密码" />
                        </div>
                        <div className={style['flex']}>
                            <div className={style['flex']}>
                                <img src={visible2?eyeo:eye} onClick={() => {
                    setVisible2(!visible2)
                }} alt="" style={{ width: '30px', marginRight: '10px' }} />
                            </div>
                        </div>
                    </div>

                </div>

                <div className={style['flex-center']}>
                    
                <div className={`${style['submit']}`} onClick={() => {
                    changeTab(1)
                }} style={{ letterSpacing: '2px' }}>
                    提交
                </div>
                </div>


            </div>


        </React.Fragment>


    )
}


export default DongTai
