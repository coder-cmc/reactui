

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import React, { Fragment, useState } from 'react'
import { Input, QRCode, Space, message } from 'antd';

import { login } from "@/apis/user.js"

const Invite = (props) => {
	const navigate = useNavigate()
	const [active,serActive]=useState(1);
	const [num,serNum]=useState(0);
	
	const changeTab=(current)=>{
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}
	
	return (
		<React.Fragment>
			<HeaderTitle title="提款申請" right_title="首页"/>
			<div className={style['main']}>
				<div className={style['withdraw_group']}>
					<div className={style['withdrawBox']}>
						<div className={`${style['wbli']} ${active === 1 ? style['active'] : ''}`} onClick={()=>{changeTab(1)}}>
							<div className={style['btn']}>轉數快提款</div>
						</div>
						<div className={`${style['wbli']} ${active === 2 ? style['active'] : ''}`} onClick={()=>{changeTab(2)}}>
							<div className={style['btn']}>銀行卡提款</div>
						</div>
						<div className={`${style['wbli']} ${active === 3 ? style['active'] : ''}`} onClick={()=>{changeTab(3)}}>
							<div className={style['btn']}>USDT-TRC20提款</div>
						</div>
						<div className={`${style['wbli']} ${active === 4 ? style['active'] : ''}`} onClick={()=>{changeTab(4)}}>
							<div className={style['btn']}>USDT-ERC20提款</div>
						</div>
					</div>
				</div>
				<div className={style['withDraw_info']}>
					{
						active == 1 ? (<ul>
							<li>
								<span className={style['left']}>姓名：</span>
								<span className={style['right']}>GUTIANLE</span>
							</li>
							<li>
								<span className={style['left']}>轉數快帳號：</span>
								<span className={style['right']}>1123</span>
							</li>
							<li>
								<span className={style['left']}>錢包餘額：</span>
								<span className={style['right']}>0.14元</span>
								<div className={style['btn']}>全部提款</div>
							</li>
							<li>
								<span className={style['left']}>尚餘打碼量：</span>
								<span className={style['right']}>0</span>
							</li>
							<li>
								<span className={style['left']}>可提款次數：</span>
								<span className={style['right']}>500</span>
							</li>
						</ul>) : ''
					}
					{
						active == 2 ? (<ul>
							<li>
								<span className={style['left']}>姓名：</span>
								<span className={style['right']}>GUTIANLE</span>
							</li>
							<li>
								<span className={style['left']}>銀行名稱：</span>
								<span className={style['right']}>003 渣打銀行(香港)有限公司</span>
							</li>
							<li>
								<span className={style['left']}>銀行帳號：</span>
								<span className={style['right']}>2323**2323</span>
							</li>
							<li>
								<span className={style['left']}>錢包餘額：</span>
								<span className={style['right']}>0.14元</span>
								<div className={style['btn']}>全部提款</div>
							</li>
							<li>
								<span className={style['left']}>尚餘打碼量：</span>
								<span className={style['right']}>0</span>
							</li>
							<li>
								<span className={style['left']}>可提款次數：</span>
								<span className={style['right']}>500</span>
							</li>
						</ul>) : ''
					}
				</div>
				<div className={style['forms']}>
					<div className={style['form-ul']}>
						<div className={style['fli']}>
							<label>提款金額</label>
							<Input defaultValue={num} />
						</div>
					</div>
					<div className={style['operate']}>
						<div className={style['tip']}>
							到帳時間：預計10分鐘內！
							<span className={style['rule']}>《提款須知》</span>
						</div>
						<div className={style['easterBtn']}>免提直充 1%贈金直接存入餘額</div>
						<div className={style['submitBtn']}>確認提款</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}


export default Invite
