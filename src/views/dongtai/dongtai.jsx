

import { Link, useNavigate } from "react-router-dom";
import HeaderTitle from "../login/header";
import style from "./index.module.less"
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons"
import React, { Fragment, useState } from 'react'
import {
	Form,
	Input,
	Checkbox,
	Button,
	Toast,

} from 'antd-mobile'
import { login } from "@/apis/user.js"

import bg from '../../static/PageAnnouncement_bg_shh.png'
import gg from '../../static/notice_icon_shh.png'
import xx from '../../static/message_icon_shh.png'

const TIME = 60000; //定义时效性


const DongTai = (props) => {
	const navigate = useNavigate()
  const [active,serActive]=useState(1);
	const [visible, setVisible] = useState(false)
	const [formData, setFormData] = useState({ name: '', password: '', checked: false })
	const [form] = Form.useForm()
	// 查看缓存中是否有账号密码
  console.log(visible,'这个是啥---')
  console.log(form,'这个是啥---')
	let formDataMemo = {}
	if(localStorage.getItem('formData')){
		formDataMemo = JSON.parse(localStorage.getItem('formData'))
	}

	if (formDataMemo.name && formDataMemo.password && formDataMemo.expirationTime && Date.now() < formDataMemo.expirationTime) {
		setFormData({
			name: formDataMemo.name,
			password: formDataMemo.password,
			checked: true,
		})

		console.log(formData, 'formData')
	}

	const onFinish = (values) => {
		console.log(values, 'values')
		/*
			1.调用登录接口
			2.登录成功之后，看其是否勾选了记住密码，如果记住密码则将账号密码缓存在内存中
			3.下次进入秘密
		*/
		login(values).then(res => {
			if (res.code == 200) {
				Toast.show({
					icon: 'success',
					content: "操作成功",
				})

				// 如果设置了记住密码，则在登录后将账号密码存储在cookie或storage中，并设置时效性以此来记住密码
				if (formData.checked) {
					localStorage.setItem('formData', JSON.stringify({
						...values,
						expirationTime: Date.now() + TIME,
					}));
				} else {
					localStorage.removeItem('formData')
				}
			} else {
				Toast.show({
					icon: 'fail',
					content: "操作失败",
				})

			}
		})

	};

	const onFinishFailed = (errorInfo) => {
		console.log('Failed:', errorInfo);
	};
	const changeTab=(current)=>{
		console.log('current:', current);
		serActive(current) //更新 active 状态
	}



	return (
		<React.Fragment>
			<HeaderTitle title="动 态" right_title="首页"/>
			<div className={style['login_box']}>
				<div style={{ textAlign: 'center',width:'100%',borderRadius:'20px'}}>
					<img src={bg} alt="" style={{ width: '100%' }}/>
				</div>
				
				<div className={style['operation2']}>
				{/* {[style['tabs'], style['active']].join(' ')} */}
				
					<div className={`${style['tabs']} ${active === 1 ? style['active'] : ''}`} onClick={()=>{
						changeTab(1)
					}}>
            <img src={gg} alt="" />
            最新公告
					</div>
					<div className={`${style['tabs']} ${active === 2 ? style['active'] : ''}`} onClick={()=>{
						changeTab(2)
					}}>
          <img src={xx} alt="" />
            我的消息
            </div>
				</div>


       <div className={style['content']}>
	   <div className={[style['operation']].join(' ')}>
					<span>
						最新公告-----
					</span>
					<span onClick={() => navigate('/emptyPage')} style={{color:'#999'}}>2023/11/06</span>
		</div>
			
			<div style={{width:'100%',height:'100px'}}></div>
	   </div>
			
		</div>
			
      

		</React.Fragment>
  

	)
}


export default DongTai
