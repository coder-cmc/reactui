import { useState } from 'react'
import Router from "@/router/index.jsx"
import Style from "./App.module.less"
console.log(Style)
function App() {
  const [count, setCount] = useState(0)

  return (
		<div className={Style['app_box']}>
			<Router />
		</div>
  )
}

export default App
