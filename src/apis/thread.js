import Request from "../utils/axios"

export function getAllThreads(data){
  return Request({
    url:"/threads/getAllThreads",
    method:"get",
    data
  })
}


export function getThreadComment(params){
  return Request({
    url:"/comments/getAllCommentsByThreadId",
    method:'get',
    params
  })
}



export function addThread(data){
  return Request({
    url:"/threads/addAThread",
    method:'post',
    data,
    params:data
  })
}
export function sendAComment(data){
  return Request({
    url:"/comments/sendAComment",
    method:'post',
    data,
    params:data,
  })
}

