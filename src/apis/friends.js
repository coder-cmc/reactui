import Request from "../utils/axios"


export function getPossibleFriends(params){
  return Request({
    url:"/friendships/getPossibleFriends",
    method:"get",
    params
  })
}




export function getPendingsList(params){
  return Request({
    url:"/friendships/getPendingsList",
    method:"get",
    params
  })
}



export function sendFriendsRequest(params){
  return Request({
    url:"/friendships/sendFriendsRequest",
    method:"get",
    params
  })
}



export function approvePendingById(params){
  return Request({
    url:"/friendships/approvePendingById",
    method:"get",
    params
  })
}



export function getFriendsList(params){
  return Request({
    url:"/friendships/getFriendsList",
    method:"get",
    params
  })
}

export function rejectFriendsRequest(params){
  return Request({
    url:"/friendships/rejectFriendsRequest",
    method:"get",
    params
  })
}



export function remove(params){
  return Request({
    url:"/friendships/unfriend",
    method:"get",
    params
  })
}








export function getFriendsRequestList(params){
  return Request({
    url:"/friendships/getFriendsRequestList",
    method:"get",
    params
  })
}





export function unfriends(params){
  return Request({
    url:"/friendships/unfriends",
    method:"DELETE",
    params,
  })
}


export function findUserReceivedRequests(params){
  return Request({
    url:"friendships/findUserReceivedRequests",
    method:"DELETE",
    params,
  })
}

