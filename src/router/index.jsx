import { useRoutes } from "react-router-dom"
import {  Navigate } from "react-router-dom";


import Login from "@/views/login/index"
import EmptyPage from "@/views/emptyPage";
import Register from "@/views/register/index";
import DongTai from "@/views/dongtai/dongtai";
import Mine from "@/views/mine/index";
import Promotionlist from "@/views/promotionlist/index";
import Newsdetail from "@/views/promotionlist/detail";
import Invite from "@/views/invite/invite";
import MoneyBao from "@/views/money_bao/index"; 
import ModifyPassword from "@/views/modify_password/index";
import ProfitLoss from "@/views/profitloss/index"
import Recharge from "@/views/record/recharge"
import CardManage from "@/views/cardmanage/cardmanage"
import DiscountApplication from "@/views/discount_application/index"; 
import RakebackAet from "@/views/rakeback_get/index"; 
import Deposit from "@/views/deposit/index";
import Withdraw from "@/views/withdraw/index";
import Card from "@/views/card/index";
import Account from "@/views/record/account";
import Myinfo from "@/views/record/myinfo";
import Agent from "@/views/agent/index";
import RechargeOrder from "@/views/recharge_order/index";
import Index from "@/views/index/index";

function Router() {
	const routes = useRoutes([
		{
			path: "/",
			element: <Navigate to="/index" />,
		},
		{
			path: '/index',
			element: <Index />,
		},
		{
			path: '/login',
			element: <Login />,
		},
		{
			path: '/login',
			element: <EmptyPage />,
		},
		{
			path: '/register',
			element: <Register />,
		},
		{
			path: '/dongtai',
			element: <DongTai />,
		},
		{
			path: '/mine',
			element: <Mine />,
		},
		{
			path: '/promotionlist',
			element: <Promotionlist />,
		},
		{
			path: '/newsdetail',
			element: <Newsdetail />,
		},
		{
			path: '/invite',
			element: <Invite />,
		},
		{  
			path: '/money_bao',  
			element: <MoneyBao />,  
		},  
		{  
			path: '/modify_password',  
			element: <ModifyPassword />,  
		},  
		{  
			path: '/profitloss',  
			element: <ProfitLoss />,  
		},  
		{  
			path: '/rechargerecord',  
			element: <Recharge />,  
		},  
		{  
			path: '/cardmanage',  
			element: <CardManage />,
		}, 
		{  
			path: '/discount_application',  
			element: <DiscountApplication />,  
		}, 
		{  
			path: '/rakeback_get',  
			element: <RakebackAet />,  
		},
		{
			path: '/deposit',  
			element: <Deposit />,  
		},
		{
			path: '/withdraw',  
			element: <Withdraw />,  
		},
		{
			path: '/card',  
			element: <Card />,  
		},
		{
			path: '/account',  
			element: <Account />,  
		},
		{
			path: '/myinfo',  
			element: <Myinfo />,  
		},
		{
			path: '/agent',  
			element: <Agent />,  
		},
		{
			path: '/rechargeorder',  
			element: <RechargeOrder />,  
		}
	])
	return routes
}

export default Router
