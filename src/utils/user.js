const user_key = 'userinfo'

export function setUserInfo(data){
  localStorage.setItem(user_key, JSON.stringify(data))
}

export function getUserInfo(){
  return JSON.parse(localStorage.getItem(user_key))
}