import axios from 'axios'


import { message } from 'antd'


// create an axios instance
const service = axios.create({
  baseURL: "/api",
  timeout: 10000 // request timeout
})

let timer

service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    message.error('bed request')
    Promise.reject(error)
  }
)


service.interceptors.response.use(
  response => {
    return response.data
  },
  err => {
   
    message.error(err.message)

    return Promise.reject(err)
  }
)

export default service
